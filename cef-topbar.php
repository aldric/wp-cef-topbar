<?php
/**
 * Plugin Name: Barre CEF
 * Plugin URI: http://wp.cef.fr/plugins/cef-topbar/
 * Description: Barre haute affichant des liens rapides et personnalisables.
 * Version: 1.0
 * Author: Aldric de Villartay
 * Author URI: http://eglise.catholique.fr
 * Text Domain: topbar
*/

/*  Copyright 2014  Aldric de Villartay  (email : dev@cef.fr)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// don't load on admin pages
if (!is_admin()) {
    
    // option to hide cef-topbar on mobile devices
    $hide_on_mobile = esc_attr( get_option( 'topbar-settings-mobile' ) );
    
    // if option checked : hide 
    if (!empty($hide_on_mobile)) {
        if ( !wp_is_mobile() ) {
            add_action('wp_footer', 'topbar_insert');
        }
    }
    // else : show always
    else {
        add_action('wp_footer', 'topbar_insert');
    }
    
}

function topbar_insert() {
    
    wp_enqueue_script('topbar-jquery', plugins_url( 'assets/jquery.1.4.1.min.js', __FILE__ ), array('jquery'), '1.4.1');
    wp_enqueue_script('topbar-jsapi',  plugins_url( 'assets/jsapi.js', __FILE__ ), false, '1.0');
    wp_enqueue_style('topbar', plugins_url( 'assets/cef-topbar.css', __FILE__ ), false, '1.0');
    
    $custom_topbar_id = esc_attr( get_option( 'topbar-settings-id' ) );
    if (!empty($custom_topbar_id)){
        $topbar_id = $custom_topbar_id;
    }
    else {
        $topbar_id = 'cef';
    }
    //default top-bar settings
    $site_search = 'true';
    $share_links = 'true';
    $add_top_margin = 'false';
    $with_animation = 'true';
    $scrolling_bar = 'true';
    
    ?>
<!-- CEF API --> 
<div id='cef-topbar-root'></div> 
<script type="text/javascript" charset="utf-8">
    window.cefAsyncInit = function() { 
    CEF.initNavigationBar({
        site_search: <?php echo $site_search; ?>,share_links: <?php echo $share_links; ?>,  add_top_margin: <?php echo $add_top_margin; ?>, with_animation: <?php echo $with_animation; ?>, scrolling_bar: <?php echo $scrolling_bar; ?>}); 
}; 
(function() { 
    var e = document.createElement('script'); e.async = true; 
    e.src = 'http://recherche.catholique.fr/api/<?php echo $topbar_id; ?>.js'; 
    document.getElementById('cef-topbar-root').appendChild(e); 
}()); 
</script>
    <?php
}

add_action('admin_menu', 'topbar_options');

function topbar_options(){
        $page_title = 'Gestion de la barre CEF';
        $menu_title = 'Barre CEF';
        $capability = 'manage_options';
        $menu_slug = 'topbar';
        add_submenu_page( 'options-general.php', $page_title, $menu_title, $capability, $menu_slug, 'topbar_settings'); 
}

function topbar_settings() {
?>
<div class="wrap">
    <?php screen_icon(); ?>
    <h2>Réglages de la barre CEF</h2>
    <form action="options.php" method="POST">
        <?php settings_fields( 'topbar-setting-group' ); ?>
        <?php do_settings_sections( 'cef-topbar' ); ?>
        <?php submit_button(); ?>
    </form>
</div>
<?php
}

add_action( 'admin_init', 'topbar_settings_init' );

function topbar_settings_init() {
    register_setting( 'topbar-setting-group', 'topbar-settings-id' );
    register_setting( 'topbar-setting-group', 'topbar-settings-mobile' );
    // Sections
    add_settings_section( 'section-intro', '', 'section_intro_callback', 'cef-topbar' );
    add_settings_section( 'section-help', 'Instructions', 'section_help_callback', 'cef-topbar' );
    add_settings_section( 'section-settings', 'Réglages', '', 'cef-topbar' );
    add_settings_section( 'section-debug', 'Debug', 'section_debug_callback', 'cef-topbar' );
    // Fields
    add_settings_field( 'field-cefapi', 'Identifiant de votre barre', 'field_cefid_callback', 'cef-topbar', 'section-settings' );
    add_settings_field( 'field-mobile', 'Option d\'affichage', 'field_mobile_callback', 'cef-topbar', 'section-settings' );
}

function section_intro_callback() {
    ?>
    <p>La barre CEF vous permet d'ajouter des liens rapides sous forme d'une barre haute (ou "top-bar") qui s'affiche tout en haut de votre site WordPress.<br />
    Cette extension va automatiquement insérer le code nécessaire à son affichage sur votre site.<br />
    <strong>Remarque : </strong>La barre affichée par défaut est celle du site <a href="http://www.eglise.catholique.fr/" target="_blank">eglise.catholique.fr</a> mais vous pouvez paramétrer votre propre barre en suivant les instructions ci-dessous. </p>
    <?php
}

function section_help_callback() {
    ?>
    <p>Vous devez au préalable récupérer l'identifiant de votre barre depuis le site <a href="http://recherche.catholique.fr/admin/">recherche.catholique.fr</a></p>
    <p>Exemple 1 : <code>/api/cef.js</code> l'identifiant est <code>cef</code>
    <p>Exemple 2 : <code>/api/diocese/paris.js</code> l'identifiant est <code>diocese/paris</code>
    <?php
}

function field_cefid_callback() {
    $topbar_id = esc_attr( get_option( 'topbar-settings-id' ) );
    echo '<input class="regular-text" type="text" name="topbar-settings-id" value="'.$topbar_id.'" />';
}
function field_mobile_callback() {
    $mobile = esc_attr( get_option( 'topbar-settings-mobile' ) );
    echo '<span><input type="checkbox" id="settings-mobile" name="topbar-settings-mobile" '.($mobile ? 'checked=""' : '' ).'>&nbsp;<label for="settings-mobile">Masquer automatiquement pour les mobiles et tablettes ?</label></span>';
} 

function section_debug_callback() {
    $topbar_id = esc_attr( get_option( 'topbar-settings-id' ) );
    if (!empty($topbar_id)){
        echo '<p>L\'adresse complète de votre barre est <a href="http://recherche.catholique.fr/api/'.$topbar_id.'.js" target="_blank"><code>http://recherche.catholique.fr/api/'.$topbar_id.'.js</code></a></br>';
        echo 'Votre barre est correctement configurée lorsque le lien ci-dessous vous affiche le code source du fichier javascript.</p>';
    }
    else {
        echo '<p>Vous devez renseigner le code de votre barre CEF pour afficher son URL.</p>';
    }
} 