# README #

La barre CEF vous permet d'ajouter des liens rapides sous forme d'une barre haute (ou "top-bar") qui s'affiche tout en haut de votre site WordPress.
Cette extension va automatiquement insérer le code nécessaire à son affichage sur votre site.
Remarque : La barre affichée par défaut est celle du site eglise.catholique.fr mais vous pouvez paramétrer votre propre barre

### Installation ###

* Envoyez les fichiers contenus dans cef-topbar dans le répertoire des extensions de WordPress /wp-content/plugins/
* Activez le plugin **Barre CEF** depuis la page des extensions

### Réglages ###

* Une page de réglages est disponible depuis le menu **Réglages > Barre CEF** 

### Auteur ###

* Aldric de Villartay - dev@cef.fr
* Conférence des évêques de France